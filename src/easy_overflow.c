#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void disable_buffering(void) {
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void alarm_handler(int sig) {
    printf("ALARM!");
    exit(0);
}

int main(void) {
    disable_buffering();
    signal(SIGALRM, alarm_handler);
    alarm(5);

    int message = 0x31337;
    char buf[100] = {0};    

    printf("Enter ♂right♂ value: ");
    read(0, buf, 0x100);

    if (message == 0xdeadb177) {
        system("cat flag");
    } else {
        printf("message = 0x%x\n", message);
        printf("Oops!!! let\'s try again\n");
    }
    exit(0);
    return 0;
}
