## Build

```bash
docker build -t "easy_overflow" .
```

DO NOT use *bin* as challenge's name

## Run

```bash
docker run -d -p "0.0.0.0:pub_port:9999" -h "easy_overflow" --name="easy_overflow" easy_overflow
```

`pub_port` is the port you want to expose to the public network.

## Capture traffic

If you want to capture challenge traffic, just run `tcpdump` on the host. Here is an example.

```bash
tcpdump -w easy_overflow.pcap -i eth0 port pub_port
```
