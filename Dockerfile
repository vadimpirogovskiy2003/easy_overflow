FROM ubuntu:22.04

RUN apt-get update && apt-get -y dist-upgrade && \
  apt-get install -y socat

RUN useradd -m ctf

WORKDIR /home/ctf

COPY ./bin/ /home/ctf/
RUN chown -R root:ctf /home/ctf && \
  chmod -R 750 /home/ctf && \
  chmod 740 /home/ctf/flag

CMD socat TCP-LISTEN:9999,reuseaddr,fork EXEC:./easy_overflow

EXPOSE 9999
